# MVA in VBSZZ

Two approaches of MVA in VBSZZ analysis:

1.Use TMVA built in BDT and Keras: 4l-TMVA.ipynb. BDT algroithem in TMVA doesn't support multi-threads, very slow.

2.Use XGBoost package and tensorflow: 4l-NN-XGBoost.ipynb. XGBoost is a popular regression tree package, which support multi-thread in tree growth. (divid spliting of each feature into different thread at each level).

XGBoost also support GPU boost. GPU histogram method could speed up tree growth dramaticly. For keras, RMSProp optimizer could provide best performance.